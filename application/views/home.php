<div class="row">
   <div class="col-md-12">
       <video muted autoplay loop poster="polina.jpg" id="bgvid">
         <source src="http://techslides.com/demos/sample-videos/small.webm" type="video/webm">
         <source src="http://techslides.com/demos/sample-videos/small.mp4" type="video/mp4">
      </video>
		
      <div class="container">
		  <h2>Main Category</h2>
		  <ul class="nav nav-pills">
		    <li class="active" >
		    	<a data-toggle="pill"  href="#clothing"><i class="fa fa-shirtsinbulk" style="margin-right: 3px;"></i> Clothings <span class="badge">90</span></a>
		    </li>		    
		    
		    <li>
		    	<a data-toggle="pill" href="#customersinthenews"><i class="fa fa-dollar" style="margin-right: 3px;"></i> Customers in the News</a>
		    </li>

		    <li>
		    	<a data-toggle="pill" href="#competitorsinthenews"><i class="fa fa-dollar" style="margin-right: 3px;"></i> Competitors in the News</a>
		    </li>
		    

		    <li>
		    	<a data-toggle="pill" href="#menu1"><i class="fa fa-dollar" style="margin-right: 3px;"></i> Walbro Products in the News</a>
		    </li>
		    <li class="submenu" >
		    	<a data-toggle="pill" href="#menu2"><i class="fa fa-dollar" style="margin-right: 3px;"></i> - none -</a>
		    </li>
			 
		  </ul>
		  
		  <div class="tab-content" >
		    <div id="clothing" class="tab-pane fade in active" >
		      <h3>Clothings</h3>
			      <ul class="nav nav-pills col-md-3" style="height: 100%;width: 230px;" >
				    <li>
				    	
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#currentmonth"><i class="fa fa-calendar-o" style="margin-right: 3px;"></i> Current month</a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#30daysago"><i class="fa fa-calendar" style="margin-right: 3px;"></i> 30 days ago</a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#60daysago"><i class="fa fa-calendar-plus-o" style="margin-right: 3px;"></i> more than 60 days ago</a>
				    </li>	
				   </ul>
			    <div class="tab-content">
			    	<div id="currentmonth" class="tab-pane fade in">
			    		<h2>Current month</h2>
			    	</div>
			    	<div id="30daysago" class="tab-pane fade in">
			    		<h2>30 days ago</h2>
			    	</div>
			    	<div id="60daysago" class="tab-pane fade in">
			    		<h2>60 days ago</h2>
			    	</div>
			    </div>	  
			</div>
		    <div id="customersinthenews" class="tab-pane fade">
		      <h3>Customers in the News</h3>		      
			    <ul class="nav nav-pills col-md-3">
			    	<li class="submenu" >
				    	<a data-toggle="pill" href="#yamahamc"><i class="fa fa-automobile" style="margin-right: 3px;"></i> yamaha motor corp. USA <span class="badge">4</span></a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#grizzly"><i class="fa fa-dollar" style="margin-right: 3px;"></i> grizzly </a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#armycio"><i class="fa fa-dollar" style="margin-right: 3px;"></i> army cio <span class="badge">1</span></a>
				    </li>
			    </ul>

			    <div class="tab-content">
			    	<div id="yamahamc" class="tab-pane fade in">
			    		<h2>yamahamc</h2>
			    	</div>
			    	<div id="grizzly" class="tab-pane fade in">
			    		<h2>grizzly</h2>
			    	</div>
			    	<div id="armycio" class="tab-pane fade in">
			    		<h2>armycio</h2>
			    	</div>
			    </div>	
		    </div>
		    <div id="competitorsinthenews" class="tab-pane fade">
		      <h3>Competitors in the News</h3>		      
			    <ul class="nav nav-pills col-md-3">
			    	<li class="submenu" >
				    	<a data-toggle="pill" href="#menu2"><i class="fa fa-dollar" style="margin-right: 3px;"></i> ashton carter (2)</a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#menu3"><i class="fa fa-dollar" style="margin-right: 3px;"></i> bug bounty program (1)</a>
				    </li>
				    <li class="submenu" >
				    	<a data-toggle="pill" href="#menu1"><i class="fa fa-dollar" style="margin-right: 3px;"></i> defense department (1)</a>
				    </li>
			    </ul>

			    <div class="tab-content">
			    	<div id="yamahamc" class="tab-pane fade in">
			    		<h2>yamahamc</h2>
			    	</div>
			    	<div id="grizzly" class="tab-pane fade in">
			    		<h2>grizzly</h2>
			    	</div>
			    	<div id="armycio" class="tab-pane fade in">
			    		<h2>armycio</h2>
			    	</div>
			    </div>	
		    </div>
		  </div>
		</div>
   </div>
</div>
<script type='text/javascript' src='<?php echo base_url('assets/js/jquery.min.js'); ?>'></script>
<script type="text/javascript">
	
	$(document).ready(function(){
		$('.nav-pills > li > a').hover(function() {
			$(this).click();
		});
	});

</script>
